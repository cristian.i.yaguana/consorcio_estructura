/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Marca;
import java.io.Serializable;

// Cola
public class MarcaCon implements Serializable{
    
    public class Nodo1 implements Serializable {

        public Marca mar;
        public Nodo1 sig;

        public Nodo1(Marca marca) {
            mar = marca;
            sig = null;
        }

        public Marca getMat() {
            return mar;
        }

        public void setMat(Marca mar) {
            this.mar = mar;
        }

        public Nodo1 getSig() {
            return sig;
        }

        public void setSig(Nodo1 sig) {
            this.sig = sig;
        }

    }

    public Nodo1 frente, fincola;
    public Nodo1 pFound;
    public Nodo1 pFound2;
    int num = 0;

    public MarcaCon() {
        fincola = null;
    }

    public void encolar(Marca marca) {
        Nodo1 nuevo = new Nodo1(marca);
        if (frente == null) {
            frente = nuevo;
        } else {
            fincola.sig = nuevo;
        }
        fincola = nuevo;
        fincola.sig = null;
    }

    public Nodo1 buscar(Nodo1 frente, String cod) {
        Nodo1 pos = frente;
        while (pos != null && !cod.equalsIgnoreCase(pos.mar.getIdMarca())) {
            pos = pos.sig;
        }
        return pos;
    }

    public Nodo1 buscar2(Nodo1 frente, String mat) {
        Nodo1 pos = frente;
        while (pos != null && !mat.equalsIgnoreCase(pos.mar.getNombre())) {
            pos = pos.sig;
        }
        return pos;
    }

    public String frente() {
        String eliminado = "";
        Nodo1 aux = frente;
        String co = aux.mar.getIdMarca();
        String n = aux.mar.getNombre();
        eliminado = co + "," + n;
        frente = frente.sig;
        return eliminado;
    }

    public Nodo1 getFrente() {
        return frente;
    }

    public void setFrente(Nodo1 frente) {
        this.frente = frente;
    }

    public Nodo1 getFincola() {
        return fincola;
    }

    public void setFincola(Nodo1 fincola) {
        this.fincola = fincola;
    }

    public Nodo1 getpFound() {
        return pFound;
    }

    public void setpFound(Nodo1 pFound) {
        this.pFound = pFound;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

}
