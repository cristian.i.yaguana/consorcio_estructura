/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Auto;
import java.io.Serializable;



public class AutoCon implements Serializable{
     
    public class Nodo1 implements Serializable {

        public Auto au;
        public Nodo1 sig;

        public Nodo1(Auto auto) {
            au = auto;
            sig = null;
        }

        public Auto getMat() {
            return au;
        }

        public void setMat(Auto aut) {
            this.au = aut;
        }

        public Nodo1 getSig() {
            return sig;
        }

        public void setSig(Nodo1 sig) {
            this.sig = sig;
        }

    }

    public Nodo1 frente, fincola;
    public Nodo1 pFound;
    public Nodo1 pFound2;
    int num = 0;

    public AutoCon() {
        fincola = null;
    }

    public void encolar(Auto auto) {
        Nodo1 nuevo = new Nodo1(auto);
        if (frente == null) {
            frente = nuevo;
        } else {
            fincola.sig = nuevo;
        }
        fincola = nuevo;
        fincola.sig = null;
    }

    public Nodo1 buscar(Nodo1 frente, String cod) {
        Nodo1 pos = frente;
        while (pos != null && !cod.equalsIgnoreCase(pos.au.getIdAuto())) {
            pos = pos.sig;
        }
        return pos;
    }

    public Nodo1 buscar2(Nodo1 frente, String mat) {
        Nodo1 pos = frente;
        while (pos != null && !mat.equalsIgnoreCase(pos.au.getModelo())) {
            pos = pos.sig;
        }
        return pos;
    }

    public String frente() {
        String eliminado = "";
        Nodo1 aux = frente;
        String co = aux.au.getIdAuto();
        String n = aux.au.getModelo();
        eliminado = co + "," + n;
        frente = frente.sig;
        return eliminado;
    }

    public Nodo1 getFrente() {
        return frente;
    }

    public void setFrente(Nodo1 frente) {
        this.frente = frente;
    }

    public Nodo1 getFincola() {
        return fincola;
    }

    public void setFincola(Nodo1 fincola) {
        this.fincola = fincola;
    }

    public Nodo1 getpFound() {
        return pFound;
    }

    public void setpFound(Nodo1 pFound) {
        this.pFound = pFound;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
