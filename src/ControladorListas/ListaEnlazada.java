package ControladorListas;

import Modelo.Persona;

/**
 *
 * @author Legion
 */
public class ListaEnlazada {

    public Nodo cabecera;

    public ListaEnlazada() {
        this.cabecera = null;
    }

    public boolean estaVacio() {
        return (this.cabecera == null);
    }

    public void imprimir() {
        if (!estaVacio()) {
            Nodo tmp = cabecera;
            while (tmp != null) {
                System.out.println(tmp.getDato());
                tmp = tmp.getSiguiente();
            }
        }
    }

    public void insertar(Persona dato) {
        Nodo tmp = new Nodo(dato, cabecera);
        cabecera = tmp;
    }

    public Persona extraer() {
        Persona dato = null;
        if (!estaVacio()) {
            dato = cabecera.getDato();
            cabecera = cabecera.getSiguiente();
        }
        return dato;
    }

    public boolean insertarDato(Persona dato) {
        try {
            insertarFinal(dato);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public Persona consultar(Integer pos) {
        Persona dato = null;
        if (!estaVacio() && (pos <= (tamanio() - 1))) {
            Nodo tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                dato = tmp.getDato();
            }
        }
        return dato;
    }

    public int tamanio() {
        int tamanio = 0;
        if (!estaVacio()) {
            Nodo tmp = cabecera;
            while (tmp != null) {
                tamanio++;
                tmp = tmp.getSiguiente();
            }
        }
        return tamanio;
    }

    public void insertar(Persona dato, int pos) {
        if (estaVacio() || pos < 0) {
            insertar(dato);
        } else {
            Nodo iterador = cabecera;
            for (int i = 0; i < pos; i++) {
                if (iterador.getSiguiente() == null) {
                    break;
                }
                iterador = iterador.getSiguiente();
            }
            Nodo tmp = new Nodo(dato, iterador.getSiguiente());
            iterador.setSiguiente(tmp);
        }
    }

    private void insertarFinal(Persona dato) {
        insertar(dato, (tamanio() - 1));
    }

}
