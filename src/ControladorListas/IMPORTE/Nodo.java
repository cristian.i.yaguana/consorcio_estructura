
package ControladorListas.IMPORTE;

import Modelo.Importacion;

/**
 *
 * @author Legion
 */
public class Nodo {
    private Importacion dato;
    private Nodo siguiente;
   
    public Nodo(Importacion dato, Nodo siguiente) {
        this.dato = dato;
        this.siguiente = siguiente;
    }

    public Nodo() {
        this.dato = null;
        this.siguiente = null;
    }

    public Importacion getDato() {
        return dato;
    }

    public void setDato(Importacion dato) {
        this.dato = dato;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    
}
