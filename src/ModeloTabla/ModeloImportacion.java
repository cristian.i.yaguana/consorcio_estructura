
package ModeloTabla;

import ControladorListas.IMPORTE.ListaEnlazada;
import Modelo.Importacion;
import javax.swing.table.AbstractTableModel;

public class ModeloImportacion extends AbstractTableModel {
    private ListaEnlazada lista;

    public ListaEnlazada getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada lista) {
        this.lista = lista;
    }
   
    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
       return lista.tamanio();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Importacion dato = lista.consultar(rowIndex);
        switch(columnIndex){
            
            case 0: return (rowIndex+1);
            case 1: return dato.getIdimportacion();
            case 2: return dato.getLugarimportacion();
            case 3: return dato.getDestinoimportacion();
            case 4: return dato.getCostoimportacion();
           
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Numero";
            case 1: return "Id Importacion";
            case 2: return "Lugar Importacion";
            case 3: return "Destino Importacion";
            case 4: return "Costo Importacion";
            
            default: return null;
        }
    }    
}
