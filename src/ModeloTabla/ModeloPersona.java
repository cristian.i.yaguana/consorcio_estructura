
package ModeloTabla;

import ControladorListas.ListaEnlazada;
import Modelo.Persona;
import javax.swing.table.AbstractTableModel;

public class ModeloPersona extends AbstractTableModel {
    private ListaEnlazada lista;

    public ListaEnlazada getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada lista) {
        this.lista = lista;
    }
   
    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
       return lista.tamanio();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Persona dato = lista.consultar(rowIndex);
        switch(columnIndex){
            
            case 0: return (rowIndex+1);
            case 1: return dato.getIdpersona();
            case 2: return dato.getNombre();
            case 3: return dato.getTelefono();
            case 4: return dato.getEmail();
            case 5: return dato.getCedula();
            default: return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Numero";
            case 1: return "Id Persona";
            case 2: return "Nombre";
            case 3: return "Telefono";
            case 4: return "Email";
            case 5: return "Cedula";
            default: return null;
        }
    }    
}
