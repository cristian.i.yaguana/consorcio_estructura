/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

public class Marca implements Serializable{
    private String IdMarca;
    private String Nombre;
    private String Fecha;

    public Marca() {
    }

    public Marca(String IdMarca, String Nombre, String Fecha) {
        this.IdMarca = IdMarca;
        this.Nombre = Nombre;
        this.Fecha = Fecha;
    }

    public final void setRegistro(Object[] registro) {
        this.Nombre = registro[0].toString();
        this.Fecha = registro[1].toString();

    }

    public Object[] getRegistro() {
        Object[] registro = {Nombre,Fecha};
        return registro;
    }
    public String getIdMarca() {
        return IdMarca;
    }

    public void setIdMarca(String IdMarca) {
        this.IdMarca = IdMarca;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }
  
}
