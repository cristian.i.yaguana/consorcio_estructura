/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USER
 */
public class Importacion {

    private int Idimportacion;
    private String Lugarimportacion;
    private String Destinoimportacion;
    private int costoimportacion;

    public int getIdimportacion() {
        return Idimportacion;
    }

    public void setIdimportacion(int Idimportacion) {
        this.Idimportacion = Idimportacion;
    }

    public String getLugarimportacion() {
        return Lugarimportacion;
    }

    public void setLugarimportacion(String Lugarimportacion) {
        this.Lugarimportacion = Lugarimportacion;
    }

    public String getDestinoimportacion() {
        return Destinoimportacion;
    }

    public void setDestinoimportacion(String Destinoimportacion) {
        this.Destinoimportacion = Destinoimportacion;
    }

    public int getCostoimportacion() {
        return costoimportacion;
    }

    public void setCostoimportacion(int costoimportacion) {
        this.costoimportacion = costoimportacion;
    }

}
