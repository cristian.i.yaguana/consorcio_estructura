/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class LoginUsuario implements Serializable{
    private String usuario;
    private String contraseña ;

    
    public final void setRegistro (Object [] registro){
        this.usuario = registro[0].toString();
        this.contraseña = registro[1].toString();
    }
   
    public Object[] getRegistro(){
        Object[] registro = {usuario, contraseña};
        return registro;
    }
    
    public LoginUsuario (Object[] registro){
        this.setRegistro(registro);
    }
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
}
