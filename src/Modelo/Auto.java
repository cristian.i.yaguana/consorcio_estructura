package Modelo;

import java.io.Serializable;
  public class Auto implements Serializable{
    
    private  String  idAuto;
    private String color;
    private String modelo;
    private int AñoFabricacion;
    private String Matricula;
    private int precio;
    private String garantia;

    public Auto() {
    }

    public Auto(String modelo, String color, int AñoFabricacion, String Matricula, int precio, String garantia) {
        this.modelo = modelo;
        this.color = color;
        this.AñoFabricacion = AñoFabricacion;
        this.Matricula = Matricula;
        this.precio = precio;
        this.garantia=garantia;
    }

   

    
    public final void setRegistro(Object[] registro) {
        this.modelo = registro[0].toString();
        this.color = registro[1].toString();
        this.AñoFabricacion = Integer.parseInt(registro[2].toString());
        this.Matricula = registro[3].toString();            
        this.precio = Integer.parseInt(registro[4].toString());
        this.garantia = registro[5].toString();
    }
    
    public String getIdAuto() {
        return idAuto;
    }

    public void setIdAuto(String idAuto) {
        this.idAuto = idAuto;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAñoFabricacion() {
        return AñoFabricacion;
    }

    public void setAñoFabricacion(int AñoFabricacion) {
        this.AñoFabricacion = AñoFabricacion;
    }

    public String getMatricula() {
        return Matricula;
    }

    public void setMatricula(String Matricula) {
        this.Matricula = Matricula;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getGarantia() {
        return garantia;
    }

    public void setGarantia(String garantia) {
        this.garantia = garantia;
    }

}