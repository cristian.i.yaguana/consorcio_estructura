/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USER
 */
public class Factura {
    private int Idventas;
    private int fechav;
    private int numeroventas;
    private int licencia;
    private String direccion;

    public int getIdventas() {
        return Idventas;
    }

    public void setIdventas(int Idventas) {
        this.Idventas = Idventas;
    }

    public int getFechav() {
        return fechav;
    }

    public void setFechav(int fechav) {
        this.fechav = fechav;
    }

    public int getNumeroventas() {
        return numeroventas;
    }

    public void setNumeroventas(int numeroventas) {
        this.numeroventas = numeroventas;
    }

    public int getLicencia() {
        return licencia;
    }

    public void setLicencia(int licencia) {
        this.licencia = licencia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
