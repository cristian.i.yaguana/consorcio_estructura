/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.UsuarioCon;
import Gestorarchivos.GestorUsuario;
import javax.swing.JOptionPane;

/**
 *
 * @author USER
 */
public class Login extends javax.swing.JFrame {

    /**
     * Creates new form Login
     */
   
  GestorUsuario gestor = new GestorUsuario();
    String FileName = "login.bin";
    UsuarioCon lista = new UsuarioCon();
    RegistroUsuarioFrm pi = new RegistroUsuarioFrm();

    /**
     * Creates new form frmLogin
     */
    public Login() {
        initComponents();
        this.setResizable(false);
        this.setTitle("SISTEMA AUTOMOVILES");
        gestor.AbrirArchivo(FileName, lista);
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        BtnIngresar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        TxtUsuario = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtContraseña = new javax.swing.JPasswordField();
        BtnRegistrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        BtnIngresar.setText("Ingresar");
        BtnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnIngresarActionPerformed(evt);
            }
        });

        jLabel1.setText("Usuario");

        jLabel2.setText("Contraseña");

        TxtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtUsuarioActionPerformed(evt);
            }
        });

        jLabel3.setBackground(new java.awt.Color(204, 204, 204));
        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(204, 0, 51));
        jLabel3.setText("Bienvenidos");

        jButton1.setText("Regresar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 138, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BtnIngresar)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(TxtUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(txtContraseña))
                        .addGap(58, 58, 58))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(BtnRegistrar)
                        .addGap(72, 72, 72)
                        .addComponent(jButton1)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel3)
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(TxtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnIngresar)
                    .addComponent(jButton1)
                    .addComponent(BtnRegistrar))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TxtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtUsuarioActionPerformed

    private void BtnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnIngresarActionPerformed
        String usuario = TxtUsuario.getText();
        String contraseña = new String(txtContraseña.getPassword());
        lista.nodo2 = pi.Buscar2(lista.inicio, contraseña);

        if (usuario.equalsIgnoreCase("") && contraseña.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "¡INGRESE UN USUARIO Y CONTRESEÑA!", "WARNING", JOptionPane.WARNING_MESSAGE);
        } else if (usuario.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "¡INGRESE UN USUARIO!", "WARNING", JOptionPane.WARNING_MESSAGE);
        } else if (contraseña.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "¡INGRESE UNA CONTRASEÑA!", "WARNING", JOptionPane.WARNING_MESSAGE);
        } else {
            lista.nodo1 = pi.Buscar(lista.inicio, usuario);
            if (lista.nodo1 != null) {
                lista.nodo1 = pi.Buscar2(lista.inicio, contraseña);
                if (lista.nodo1 != null) {
                    JOptionPane.showMessageDialog(this, "¡BIENVENIDO!: " + usuario, "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
                    Principal ventanaPrincipal = new Principal();
                    ventanaPrincipal.setVisible(true);
                    dispose();

                } else {
                    JOptionPane.showMessageDialog(this, "¡CONTRASEÑA INVALIDA!: ", "WARNING", JOptionPane.WARNING_MESSAGE);
                }
            } else if (lista.nodo2 != null) {
                JOptionPane.showMessageDialog(this, "USUARIO INCORRECTO", "WARNING", JOptionPane.WARNING_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "USUARIO Y CONTRASEÑA INCORRECTO", "WARNING", JOptionPane.WARNING_MESSAGE);
                TxtUsuario.setText("");
                txtContraseña.setText("");
                TxtUsuario.requestFocus();
            }
        }

    }//GEN-LAST:event_BtnIngresarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.setVisible(false);
        this.dispose();
        Rol pr = new Rol();
        pr.setVisible(true);
        pr.toFront();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed
         String usuario = TxtUsuario.getText();
        String contraseña = new String(txtContraseña.getPassword());
        lista.nodo2 = pi.Buscar2(lista.inicio, contraseña);

        if (usuario.equalsIgnoreCase("") && contraseña.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "¡INGRESE UN USUARIO Y CONTRESEÑA!", "WARNING", JOptionPane.WARNING_MESSAGE);
        } else if (usuario.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "¡INGRESE UN USUARIO!", "WARNING", JOptionPane.WARNING_MESSAGE);
        } else if (contraseña.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "¡INGRESE UNA CONTRASEÑA!", "WARNING", JOptionPane.WARNING_MESSAGE);
        } else {
            lista.nodo1 = pi.Buscar(lista.inicio, usuario);
            if (lista.nodo1 != null) {
                lista.nodo1 = pi.Buscar2(lista.inicio, contraseña);
                if (lista.nodo1 != null) {
                    JOptionPane.showMessageDialog(this, "¡BIENVENIDO!: " + usuario, "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
                   RegistroUsuarioFrm pr = new RegistroUsuarioFrm();
                    pr.setVisible(true);
                  pr.toFront();
                } else {
                    JOptionPane.showMessageDialog(this, "¡CONTRASEÑA INVALIDA!: ", "WARNING", JOptionPane.WARNING_MESSAGE);
                }
            } else if (lista.nodo2 != null) {
                JOptionPane.showMessageDialog(this, "USUARIO INCORRECTO", "WARNING", JOptionPane.WARNING_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "USUARIO Y CONTRASEÑA INCORRECTO", "WARNING", JOptionPane.WARNING_MESSAGE);
                TxtUsuario.setText("");
                txtContraseña.setText("");
                TxtUsuario.requestFocus();
            }
        }

        
//        this.setVisible(false);
//        this.dispose();
//        RegistroUsuarioFrm pr = new RegistroUsuarioFrm();
//        pr.setVisible(true);
//        pr.toFront();
    }//GEN-LAST:event_BtnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnIngresar;
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JTextField TxtUsuario;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField txtContraseña;
    // End of variables declaration//GEN-END:variables
}
